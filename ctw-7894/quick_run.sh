#create venv
python3 -m venv ./venv
#activate venv
source ./venv/bin/activate
#install packages
pip install -r requirements.txt
#add kernel
python -m ipykernel install --user --name ${PWD##*/}