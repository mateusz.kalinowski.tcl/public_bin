import os
from pathlib import Path

from torchvision import transforms

from ipc.datasets.dataset_type import DatasetType

bucket = 's3://maka-fire-det'
role = 'arn:aws:iam::686422572251:role/mateusz.kalinowski.sagemaker_test'

data_file_name = 'custon_data.zip'
fast_dev_run = True
gpus = 0

data_dir = Path('../data')
# main directories
input_data_folder = data_dir / 'input'
byproducts_data_folder = data_dir / 'byproducts'
output_data_folder = data_dir / 'output'

pipeline_testing_folder_name = 'pipeline_test_data'
pipeline_testing_dataset_path = byproducts_data_folder / pipeline_testing_folder_name

custom_input_data_folder = input_data_folder / 'custom'
destination_zip_data_path = custom_input_data_folder / data_file_name

external_input_data_folder = input_data_folder / 'external'

dataset_path = custom_input_data_folder

# only testes images
random_state = 11022021

trial_byproducts_path = byproducts_data_folder / str(random_state)
trial_results_path = trial_byproducts_path / 'trials'
logging_dir = trial_byproducts_path / 'logs'

if not os.path.exists(trial_byproducts_path):
    os.makedirs(trial_byproducts_path)

train_data_directory = trial_byproducts_path / 'train'
val_data_directory = trial_byproducts_path / 'test'

annotations_file_name = 'annotations.csv'
annotations_path = trial_byproducts_path / annotations_file_name

trial_input_path = input_data_folder / str(random_state)
trial_output_path = output_data_folder / str(random_state)
trial_models_path = trial_output_path / 'models'

dataset_zip_name = f'{random_state}.zip'
current_model_path = trial_models_path / 'model_sagemaker.pth'


def get_transformations(dataset_type, image_size, greyscale_conversion=False):
    if dataset_type is DatasetType.train:
        transform_ops = [
            transforms.Resize((image_size, image_size)),
            transforms.RandomHorizontalFlip(),
            transforms.RandomAffine(25, translate=(0.1, 0.1), scale=(0.9, 1.1), shear=8),
            transforms.ToTensor(),
            transforms.ColorJitter(),
            # transforms.RandomErasing(p=0.5, scale=(0.02, 0.25)),
        ]
    else:
        transform_ops = [
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),
        ]
    if greyscale_conversion:
        return [transforms.Grayscale(),
                *transform_ops,
                transforms.Normalize(
                    mean=[0.462],
                    std=[0.270]
                )]

    return [*transform_ops,
            transforms.Normalize(
                mean=[0.470, 0.460, 0.455],
                std=[0.267, 0.266, 0.270]
            )]
