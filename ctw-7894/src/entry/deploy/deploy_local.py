import torch

from entry.train.estimator import fit_estimator
from ipc.datasets.dataset_type import DatasetType

if __name__ == '__main__':
    estimator = fit_estimator(dataset_type=DatasetType.pipeline_test, instance_type='local')

    predictor = estimator.deploy(initial_instance_count=1, instance_type='local')
    random_image = torch.rand(1, 3, 224, 224)
    predictor.predict(random_image)
