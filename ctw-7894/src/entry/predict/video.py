import os

import imageio
import numpy as np
from tqdm import tqdm

from config import current_model_path, trial_output_path, trial_input_path
from ipc.predictor.load_model import load_efficient_net_model
from ipc.predictor.real_time_predictor import RealTimePredictor

videos_root = trial_input_path / 'test_videos'
output_video_folder = trial_output_path / 'annotated_videos'


def annotate(predictor, input_video_path, output_file_path):
    reader = imageio.get_reader(input_video_path, format='FFMPEG')
    w = imageio.get_writer(output_file_path, format='FFMPEG', mode='I', fps=reader.get_meta_data()['fps'])

    for frame_idx, im in tqdm(enumerate(reader), total=reader.count_frames()):
        img = predictor.annotate(im)
        w.append_data(np.array(img))


if __name__ == '__main__':
    videos_available = os.listdir(videos_root)
    video_name = videos_available[1]
    input_video_path = videos_root / video_name
    print(input_video_path)

    loaded_model, transform_ops = load_efficient_net_model(current_model_path)
    predictor = RealTimePredictor(loaded_model, transform_ops)

    output_video_folder.mkdir(parents=True, exist_ok=True)
    output_file_path = output_video_folder / video_name

    annotate(predictor, input_video_path, output_file_path)
