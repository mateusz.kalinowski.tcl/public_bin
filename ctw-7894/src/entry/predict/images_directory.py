from pathlib import Path

from config import current_model_path
from ipc.predictor.folder_predictor import FireFolderPredictor
from ipc.predictor.load_model import load_efficient_net_model
from ipc.predictor.utils import annotate_files_in_folder

if __name__ == '__main__':
    loaded_model, transform_ops = load_efficient_net_model(current_model_path)
    assert not loaded_model.training

    notebooks_folder = Path('../notebooks')
    input_images_directory = notebooks_folder / 'test_images'
    output_images_directory = notebooks_folder / 'annotated_images'

    annotations = annotate_files_in_folder(input_images_directory)

    predictor = FireFolderPredictor(loaded_model, transform_ops, input_images_directory, annotations)
    predictor.annotate_save_images(output_folder=output_images_directory, output_image_size=(500, 500))
    print(predictor.results)
