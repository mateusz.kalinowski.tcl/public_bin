import cv2

from config import current_model_path
from ipc.predictor.load_model import load_efficient_net_model
from ipc.predictor.real_time_predictor import RealTimePredictor

if __name__ == '__main__':
    loaded_model, transform_ops = load_efficient_net_model(current_model_path)
    assert not loaded_model.training

    predictor = RealTimePredictor(loaded_model, transform_ops)

    vid = cv2.VideoCapture(0)
    while True:
        ret, frame = vid.read()
        cv2.imshow('frame', predictor.annotate(frame))

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        # time.sleep(1)
