from entry.train.estimator import fit_estimator
from ipc.datasets.dataset_type import DatasetType

if __name__ == '__main__':
    fit_estimator(dataset_type=DatasetType.train, instance_type='ml.m5.large')
