from sagemaker.pytorch import PyTorch

from config import role, bucket, pipeline_testing_folder_name, annotations_file_name, random_state
from ipc.datasets.dataset_type import DatasetType


def fit_estimator(dataset_type: DatasetType, instance_type='local', source_dir='.',
                  entry_point='entry/train/sagemaker_entrypoint.py'):
    estimator = PyTorch(
        entry_point=entry_point,
        source_dir=source_dir,
        role=role,
        framework_version='1.6.0',
        py_version="py3",
        instance_count=1,
        instance_type=instance_type,
        hyperparameters={
            'epochs': 20,
            'batch-size': 80
        })
    if dataset_type == DatasetType.pipeline_test:
        fit_input = {
            'train': bucket + f'/{pipeline_testing_folder_name}/train',
            'test': bucket + f'/{pipeline_testing_folder_name}/test',
            'annotations': f'{bucket}/{pipeline_testing_folder_name}/{annotations_file_name}'
        }
    else:
        fit_input = {
            'train': f'{bucket}/{random_state}/train',
            'test': f'{bucket}/{random_state}/test',
            'annotations': f'{bucket}/{random_state}/{annotations_file_name}'
        }

    estimator.fit(fit_input)
    return estimator
