import argparse
import os
from pathlib import Path

import pandas as pd
import pytorch_lightning as pl
import torch

from config import get_transformations, annotations_file_name, fast_dev_run, gpus
from ipc.datasets.dataset_type import DatasetType
from ipc.datasets.image_dataset import ImageDatasetSettings, LightningImageDataset
from ipc.models import EfficientNet
from ipc.models.efficient_net.implementations import EfficientNets
from ipc.models.efficient_net.main import EfficientNetTrialInfo
from ipc.structure.trial_info import TrialMode

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # hyperparameters sent by the client are passed as command-line arguments to the script.
    parser.add_argument('--epochs', type=int, default=20)
    parser.add_argument('--batch-size', type=int, default=64)
    # parser.add_argument('--gpus', type=int, default=1)  # used to support multi-GPU or CPU train

    # Data, model, and output directories. Passed by sagemaker with default to os env variables
    parser.add_argument('-o', '--output-data-dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    parser.add_argument('-m', '--model-dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('-tr', '--train', type=str, default=os.environ['SM_CHANNEL_TRAIN'])
    parser.add_argument('-te', '--test', type=str, default=os.environ['SM_CHANNEL_TEST'])
    parser.add_argument('-a', '--annotations', type=str, default=os.environ['SM_CHANNEL_ANNOTATIONS'])

    args, _ = parser.parse_known_args()
    print(args)

    trial_info = EfficientNetTrialInfo(
        trial_root_path=Path(args.output_data_dir),
        epochs=args.epochs,
        batch_size=args.batch_size,
        initial_lr=1e-3,
        optimizer=torch.optim.AdamW,
        num_classes=2,
        in_channels=3,
        loss=torch.nn.CrossEntropyLoss(),
        load_weights=True,
        adv_prop=False,
        freeze_pretrained_weights=False,
        optimizer_settings=dict(),
        scheduler_settings=dict(patience=3),
        custom_dropout_rate=None,
        trial_mode=TrialMode.train

    )
    print(trial_info.get_trial_info())

    local_annotations_file_path = f'{args.annotations}/{annotations_file_name}'

    annotations = pd.read_csv(local_annotations_file_path).rename(columns={'new_file_name': 'file_name'})

    train_annotations = annotations[annotations.is_test == 0]
    test_annotations = annotations[annotations.is_test == 1]

    model = EfficientNet(net_info=EfficientNets.b0.value,
                         trial_info=trial_info)

    # efficient nets set image size for specific architecture
    image_size = model.image_size

    train_dataset_settings = ImageDatasetSettings(args.train, image_size, train_annotations,
                                                  DatasetType.train,
                                                  transformations=get_transformations(DatasetType.train, image_size))
    val_dataset_settings = ImageDatasetSettings(args.test, image_size, test_annotations,
                                                DatasetType.val,
                                                transformations=get_transformations(DatasetType.val, image_size))

    dataset = LightningImageDataset(train_dataset_settings,
                                    val_dataset_settings,
                                    batch_size=trial_info.batch_size)

    trainer = pl.Trainer(max_epochs=trial_info.epochs,
                         gpus=gpus,
                         default_root_dir=args.output_data_dir,
                         fast_dev_run=fast_dev_run
                         )

    print('Everything is loaded. Training.')

    trainer.fit(model, datamodule=dataset)

    # After model has been trained, save its state into model_dir which is then copied to back S3
    with open(os.path.join(args.model_dir, f'model.pth'), 'wb') as f:
        torch.save(model.state_dict(), f)
