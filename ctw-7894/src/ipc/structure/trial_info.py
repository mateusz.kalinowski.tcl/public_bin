import dataclasses
from dataclasses import dataclass
from enum import Enum, auto
from pathlib import Path
from typing import Type

from torch import nn
from torch.optim import Optimizer


class TrialMode(Enum):
    train = auto()
    test = auto()


@dataclass
class TrialInfo:
    trial_root_path: Path
    epochs: int
    batch_size: int
    initial_lr: float
    optimizer: Type[Optimizer]

    num_classes: int
    in_channels: int
    loss: nn.Module
    trial_mode: TrialMode

    def get_trial_info(self):
        return dataclasses.asdict(self)
