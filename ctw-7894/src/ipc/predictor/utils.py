import os

import pandas as pd
import torch
from PIL import Image
from torchvision.transforms import transforms


def preprocess_predictions(preds):
    results = pd.DataFrame(preds[1].detach().numpy(), columns=['pred_0', 'pred_1'])
    results['actual'] = preds[0].detach().numpy().astype(bool)
    results['pred'] = preds[2].detach().numpy().astype(bool)
    results['model_right'] = results.actual == results.pred

    return results


def annotate_files_in_folder(folder_prefix):
    """
    Folder structure should be
        folder_prefix/1 - positive images
        folder_prefix/0 - negative images
    """
    results = pd.DataFrame()
    for label in [0, 1]:
        folder = folder_prefix / str(label)
        df = pd.DataFrame({'file_name': os.listdir(folder), 'label': label})
        df.file_name = df.label.apply(str) + '/' + df.file_name
        results = pd.concat([df, results])

    return results[~results.file_name.str.contains('.DS_Store')]


def quick_predict(numpy_image, model, transform_ops):
    image = Image.fromarray(numpy_image).convert('RGB')
    im_transformed = transforms.Compose(transform_ops)(image).reshape([1, 3, 224, 224])
    with torch.no_grad():
        return model(im_transformed).detach().numpy()