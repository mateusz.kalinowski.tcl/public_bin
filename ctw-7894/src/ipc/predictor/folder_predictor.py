import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from scipy.special import softmax
from torch.utils.data import DataLoader

from config import get_transformations
from ipc.datasets.dataset_type import DatasetType
from ipc.datasets.image_dataset import ImageDatasetSettings, ImageDataset
from ipc.predictor.predictor import Predictor
from ipc.predictor.utils import preprocess_predictions
from ipc.utils.annotate import put_text_on_img


def get_fire_probability(row):
    predictions = row[['pred_0', 'pred_1']].to_list()
    fire_probability = round(softmax(predictions)[1] * 100)
    return fire_probability


def save_image(image, output_folder_prefix, row):
    file_path = output_folder_prefix / f"{row.file_name.split('.')[0].split('/')[1]}.jpg"
    Image.fromarray(image).save(file_path, 'JPEG')


class FolderPredictor(Predictor):
    def __init__(self, model, transform_ops, folder_prefix, annotations, image_size=224, num_workers=4, batch_size=64,
                 merge_annotations=True):
        super().__init__(model, transform_ops)
        self.folder_prefix = folder_prefix
        self.annotations = annotations
        self.image_size = image_size
        self.data_loader = self._get_dataloader_for_folder(num_workers, batch_size)
        self.results = self.run_prediction_pipeline_on_folder(merge_annotations)

    def _get_dataloader_for_folder(self, num_workers, batch_size):
        val_dataset_settings = ImageDatasetSettings(self.folder_prefix, self.image_size, self.annotations,
                                                    DatasetType.val,
                                                    transformations=get_transformations(DatasetType.val,
                                                                                        self.image_size))

        return DataLoader(ImageDataset(val_dataset_settings),
                          batch_size=batch_size,
                          shuffle=False,
                          pin_memory=True,
                          num_workers=num_workers)

    def run_prediction_pipeline_on_folder(self, merge_annotations):
        results = preprocess_predictions(self.predict(self.data_loader))
        if merge_annotations:
            return pd.concat([results, self.annotations.reset_index(drop=True)], axis=1)
        return results


class FireFolderPredictor(FolderPredictor):
    def __init__(self, model, transform_ops, folder_prefix, annotations, image_size=224, num_workers=4,
                 batch_size=64, merge_annotations=True):
        super().__init__(model, transform_ops, folder_prefix, annotations, image_size, num_workers,
                         batch_size, merge_annotations)

    def annotate_save_images(self, output_folder, output_image_size=(600, 600), show_images=False):
        for idx, row in self.results.iterrows():
            fire_probability = get_fire_probability(row)
            image = self._process_save_image(fire_probability, output_folder, row, output_image_size)
            if show_images:
                _ = plt.figure(figsize=(10, 10))
                _ = plt.imshow(image)
                _ = plt.axis('off')

    def _process_save_image(self, fire_probability, output_folder, row, output_image_size):
        output_folder.mkdir(parents=True, exist_ok=True)

        image = self._load_image(row, output_image_size)
        image = self._annotate_image(fire_probability, image)
        save_image(image, output_folder, row)
        return image

    def _annotate_image(self, fire_probability, image):
        annotation = f'Fire probability: {fire_probability}'
        image = put_text_on_img(image, annotation,
                                font_color=(255, 0, 0) if fire_probability > 50 else (255, 255, 255))
        return image

    def _load_image(self, row, output_image_size):
        file_path = self.folder_prefix / row.file_name
        image = np.asarray(Image.open(file_path).convert('RGB').resize(output_image_size))
        return image
