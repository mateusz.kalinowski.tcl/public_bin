import numpy as np

from ipc.predictor.predictor import Predictor
from ipc.utils.annotate import put_text_on_img


class RealTimePredictor(Predictor):
    def __init__(self, model, transform_ops, modulo_val=100):
        super().__init__(model, transform_ops)
        self.iterator = 0
        self.modulo_val = modulo_val
        self.historical_predictions = np.zeros(modulo_val)

    def annotate(self, frame):
        exact_predictions, fire_probability = self.predict_frame(frame)
        self._append_historical_predictions(fire_probability)

        historical_running_mean = self.historical_predictions.mean()
        annotation = f'Fire probability: {fire_probability} ({historical_running_mean})'
        return put_text_on_img(frame, annotation,
                               font_color=(0, 0, 255) if historical_running_mean > 50 else (255, 255, 255))

    def _append_historical_predictions(self, fire_probability):
        self.historical_predictions[self.iterator % self.modulo_val] = fire_probability
        self.iterator += 1
