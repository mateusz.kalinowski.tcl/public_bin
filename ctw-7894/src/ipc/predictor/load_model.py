import torch

from config import get_transformations, trial_results_path
from ipc.datasets.dataset_type import DatasetType
from ipc.models import EfficientNet
from ipc.models.efficient_net.implementations import EfficientNets
from ipc.models.efficient_net.main import EfficientNetTrialInfo
from ipc.structure.trial_info import TrialMode


def load_efficient_net_model(model_path):
    # TODO: Add loading model from trial info.
    trial_info = EfficientNetTrialInfo(
        trial_root_path=trial_results_path,
        epochs=3,
        batch_size=16,
        initial_lr=1e-3,
        optimizer=torch.optim.AdamW,
        num_classes=2,
        in_channels=3,
        loss=torch.nn.CrossEntropyLoss(),
        load_weights=True,
        adv_prop=False,
        freeze_pretrained_weights=False,
        optimizer_settings=dict(),
        scheduler_settings=dict(patience=3),
        custom_dropout_rate=None,
        trial_mode=TrialMode.test
    )
    loaded_model = EfficientNet(net_info=EfficientNets.b0.value,
                                trial_info=trial_info)

    loaded_model.load_state_dict(torch.load(model_path))
    image_size = loaded_model.image_size
    loaded_model = loaded_model.eval()
    transform_ops = get_transformations(DatasetType.val, image_size)
    return loaded_model, transform_ops
