import torch
from scipy.special import softmax

from ipc.predictor.utils import quick_predict


class Predictor:
    def __init__(self, model, transform_ops):
        self.model = model
        self.transform_ops = transform_ops

        assert not model.training, 'Please set your model to evaluation mode'

    def predict_frame(self, frame):
        pred = quick_predict(frame, self.model, self.transform_ops)[0]
        return pred, int(softmax(pred)[1] * 100)

    def predict(self, data_loader):
        target = torch.tensor([], dtype=torch.long, device=self.model.device)
        pred = torch.tensor([], device=self.model.device)

        with torch.no_grad():
            for data in data_loader:
                inputs = [i.to(self.model.device) for i in data[:-1]]
                labels = data[-1].to(self.model.device)

                outputs = self.model(*inputs)
                target = torch.cat((target, labels), 0)
                pred = torch.cat((pred, outputs), 0)

        _, pred_class = torch.max(pred, 1)
        return target, pred, pred_class