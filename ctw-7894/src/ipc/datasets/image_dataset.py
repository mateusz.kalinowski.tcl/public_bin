import os
from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional

import pandas as pd
import torch
from PIL import Image
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms

from ipc.datasets.dataset_type import DatasetType
from ipc.utils.default_logging import configure_default_logging

log = configure_default_logging(__name__)


@dataclass
class ImageDatasetSettings:
    data_directory: Path
    image_size: int
    annotations: pd.DataFrame
    dataset_type: DatasetType
    transformations: Optional[List[torch.nn.Module]]
    in_channels: int = 3

    def __post_init__(self):
        assert 'file_name' in self.annotations.columns


class ImageDataset(Dataset):
    def __init__(self, dataset_settings: ImageDatasetSettings):
        self.dataset_settings = dataset_settings

        self.transformations = dataset_settings.transformations
        self.data_directory = dataset_settings.data_directory
        self.annotations = dataset_settings.annotations

    def __post_init__(self):
        log.info(f"Initialized dataset_type: {self.dataset_settings.dataset_type} "
                 f"from: {self.dataset_settings.data_directory}; image size: {self.dataset_settings.image_size}"
                 f"Transformations of the images are {'on' if self.transformations else 'off'}")

    def transform(self, image):
        return transforms.Compose(self.transformations)(image)

    def load_transform(self, image_file_name):
        image_fp = os.path.join(self.data_directory, image_file_name)
        image = Image.open(image_fp).convert('RGB')
        if self.transformations:
            image = self.transform(image)
        return image

    def __len__(self):
        return len(self.annotations.file_name)

    def __getitem__(self, index):
        file_name = self.annotations.file_name.iloc[index]
        image = self.load_transform(image_file_name=file_name)

        labels = self.annotations[self.annotations.file_name == file_name].label.values
        if len(labels) > 1:
            log.warn(f'Found multiple images with file_name: {file_name} (will pick first one)')
        return image, torch.as_tensor(labels[0], dtype=torch.long)


class LightningImageDataset(LightningDataModule):

    def __init__(self, train_image_dataset_settings: ImageDatasetSettings,
                 val_image_dataset_settings: ImageDatasetSettings,
                 batch_size,
                 shuffle=True,
                 pin_memory=True,
                 num_workers=4):
        super().__init__()
        self.train_dataset_settings = train_image_dataset_settings
        self.val_dataset_settings = val_image_dataset_settings

        self.batch_size = batch_size
        self.shuffle = shuffle
        self.pin_memory = pin_memory
        self.num_workers = num_workers

        self.train_data = ImageDataset(self.train_dataset_settings)
        self.val_data = ImageDataset(self.val_dataset_settings)

    def train_dataloader(self):
        return DataLoader(self.train_data,
                          batch_size=self.batch_size,
                          shuffle=self.shuffle,
                          pin_memory=self.pin_memory,
                          num_workers=self.num_workers)

    def val_dataloader(self):
        return DataLoader(self.val_data,
                          batch_size=self.batch_size,
                          shuffle=False,
                          pin_memory=self.pin_memory,
                          num_workers=self.num_workers)
