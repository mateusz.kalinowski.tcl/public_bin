from enum import Enum, auto


class DatasetType(Enum):
    train = auto()
    val = auto()
    test = auto()
    pipeline_test = auto()