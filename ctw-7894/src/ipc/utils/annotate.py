import cv2


def get_optimal_font_scale(text, font, thickness, width):
    for scale in reversed(range(0, 60, 1)):
        textSize = cv2.getTextSize(text, fontFace=font, fontScale=scale / 10, thickness=thickness)
        new_width = textSize[0][0]
        if (new_width <= width):
            return scale / 10


def put_text_on_img(img, text, font_color, thickness=2):
    font = cv2.FONT_HERSHEY_PLAIN

    rectangle_bgr = (0, 0, 0)

    font_scale = get_optimal_font_scale(text, font, thickness, width=img.shape[1] / 2)
    (text_width, text_height) = cv2.getTextSize(text, font, fontScale=font_scale, thickness=thickness)[0]

    text_offset_x = 10
    text_offset_y = img.shape[0] - 25
    # make the coords of the box with a small padding of two pixels
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height - 2))
    _ = cv2.rectangle(img, box_coords[0], box_coords[1], rectangle_bgr, cv2.FILLED)
    _ = cv2.putText(img, text, (text_offset_x, text_offset_y), font, fontScale=font_scale, color=font_color,
                    thickness=thickness)
    return img