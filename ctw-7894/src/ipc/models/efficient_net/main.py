from dataclasses import dataclass
from typing import Optional, Dict

import pytorch_lightning as pl
from torch import nn
from torch.utils import model_zoo

from ipc.models.efficient_net.Swish import Swish
from ipc.models.efficient_net.block_decoder import BlockDecoder
from ipc.models.efficient_net.block_params import round_filters
from ipc.models.efficient_net.conv_2d import get_same_padding_conv2d
from ipc.models.efficient_net.mb_conv_block import MBConvBlock
from ipc.models.efficient_net.structure import EfficientNetInfo
from ipc.structure.trial_info import TrialInfo


@dataclass
class EfficientNetTrialInfo(TrialInfo):
    load_weights: bool
    adv_prop: bool
    freeze_pretrained_weights: bool

    optimizer_settings: Dict
    scheduler_settings: Dict
    custom_dropout_rate: Optional[float]


class EfficientNet(pl.LightningModule):

    def __init__(self, net_info: EfficientNetInfo, trial_info: EfficientNetTrialInfo):
        super().__init__()
        self.net_info = net_info
        self.trial_info = trial_info
        self.image_size = net_info.network_params.compound_scalars.resolution
        self.loss = trial_info.loss

        global_params = net_info.network_params.global_params
        Conv2d = get_same_padding_conv2d(image_size=self.image_size)

        out_channels = round_filters(32, net_info.network_params)
        self._conv_stem = Conv2d(in_channels=trial_info.in_channels, kernel_size=3, stride=2,
                                 out_channels=out_channels, bias=False)
        self._bn0 = nn.BatchNorm2d(num_features=out_channels, momentum=global_params.batch_norm_momentum,
                                   eps=global_params.batch_norm_epsilon)

        self._blocks = self._build_blocks()

        out_channels = round_filters(1280, net_info.network_params)
        self._conv_head = Conv2d(in_channels=self._blocks[-1]._project_conv.out_channels, out_channels=out_channels,
                                 kernel_size=1, stride=1, bias=False)
        self._bn1 = nn.BatchNorm2d(num_features=out_channels, momentum=global_params.batch_norm_momentum,
                                   eps=global_params.batch_norm_epsilon)

        self._avg_pooling = nn.AdaptiveAvgPool2d(1)
        if trial_info.custom_dropout_rate is not None:
            self._dropout = nn.Dropout(trial_info.custom_dropout_rate)
        else:
            self._dropout = nn.Dropout(global_params.dropout_rate)
        self._classification = nn.Linear(out_channels, trial_info.num_classes)
        self._swish = Swish()

        if trial_info.load_weights:
            self._weights_update(trial_info.adv_prop, trial_info.freeze_pretrained_weights)

    def _weights_update(self, adv_prop, freeze_pretrained_weights):
        model_dict = self.state_dict()

        pretrained_dict = model_zoo.load_url(self.net_info.get_pretrained_url(adv_prop))
        pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
        if freeze_pretrained_weights:
            self._freeze_weights(pretrained_dict)
        model_dict.update(pretrained_dict)
        self.load_state_dict(model_dict)

    def _freeze_weights(self, pretrained_dict):
        for name, param in self.named_parameters():
            if name in pretrained_dict.keys():
                param.requires_grad = False

    def _build_blocks(self):
        global_params = self.net_info.network_params.global_params

        blocks = nn.ModuleList([])
        for block_args in BlockDecoder.decode(self.net_info.block_args):
            block_args = block_args.round_block(network_params=self.net_info.network_params)

            blocks.append(MBConvBlock(block_args, global_params, self.image_size))
            if block_args.num_repeat > 1:
                block_args = block_args.update_parameters(input_filters=block_args.output_filters, stride=1)
            for _ in range(block_args.num_repeat - 1):
                blocks.append(MBConvBlock(block_args, global_params, self.image_size))
        return blocks

    def _extract_features(self, inputs):
        """ Returns output of the final convolution layer """

        # Stem
        x = self._swish(self._bn0(self._conv_stem(inputs)))

        # Blocks
        for idx, block in enumerate(self._blocks):
            drop_connect_rate = self.net_info.network_params.global_params.drop_connect_rate
            if drop_connect_rate:
                drop_connect_rate *= float(idx) / len(self._blocks)
            x = block(x, drop_connect_rate=drop_connect_rate)

        # Head
        x = self._swish(self._bn1(self._conv_head(x)))

        return x

    def forward(self, inputs):
        """ Calls _extract_features to extract features, applies final linear layer, and returns logits. """
        bs = inputs.size(0)
        # Convolution layers
        x = self._extract_features(inputs)

        # Pooling and final linear layer
        x = self._avg_pooling(x)
        x = x.view(bs, -1)
        x = self._dropout(x)
        # x = self._swish(self._fc(x))
        x = self._classification(x)
        return x

    def training_step(self, batch, batch_idx):
        x, y = batch
        pred = self(x)
        loss = self.loss(pred, y)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        pred = self(x)
        loss = self.loss(pred, y)
        return loss

    def configure_optimizers(self):
        return self.trial_info.optimizer(self.parameters(), lr=self.trial_info.initial_lr,
                                         **self.trial_info.optimizer_settings)

    # def validation_epoch_end(self, outputs):
    #     val_loss_mean = 0
    #     for output in outputs:
    #         val_loss = output['val_loss']
    #         if self.trainer.use_dp or self.trainer.use_ddp2:
    #             val_loss = torch.mean(val_loss)
    #         val_loss_mean += val_loss
    #     val_loss_mean /= len(outputs)
    #     tqdm_dict = {'val_loss': val_loss_mean}
    #     return OrderedDict({'val_loss': val_loss_mean, 'progress_bar': tqdm_dict, 'log': tqdm_dict})
    #
    # def validation_end(self, outputs):
    #     return self.validation_epoch_end(outputs)
