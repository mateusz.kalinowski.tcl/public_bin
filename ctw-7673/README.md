There are differences in bucket com.tcl.bp.cn.raw events and dm.dmt_funnel results. Dmt_funnel should have the same counts as those calculated based on raw events.

It looks like there can be a bug in [spark code](https://gitlab.com/tcl-research/cloud/business-services/user-behaviour-data-pipeline) that is processing raw events and filling dmt_funnel table
To know more please go to the notebooks directory.

To reproduce:
- run commands below in ticket root
 ```bash
#create venv
python3 -m venv ./venv
#activate venv
source ./venv/bin/activate
#install packages
pip install -r requirements.txt
#add kernel to jupyter 
python -m ipykernel install --user --name ctw-7673
```
- open jupyter notebook, and file 1_s3_raw_vs_redshift.ipynb. You can use command below.
```bash
jupyter lab
```

Other info:
- if jupyter does not select created kernel (ctw-7673) by default, please select it from the top rigt corner
- if you would like to reproduce the results and run the code that connects to the redshift you need to fill the variable **AWS_SECRETS_MANAGER_ARN** with ARN to secrets manager with production db credentials.
    - additionally: you need to configure aws credentials and set **AWS_PROFILE** env variable.


links:
- [connected jira ticket](https://jira.tclking.com/browse/CTW-7673)